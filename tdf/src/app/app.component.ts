import { Component } from '@angular/core';
import { EnrollmentService } from './enrollment.service';
import { User } from './user';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  topics:any = ['Angular','React','Vue'];
  topicHasError:boolean = true;
  errorMsg = "";

  constructor(private enrollmentService:EnrollmentService){}

  userModel = new User('Syam','Syam@test.com', 9181716151, 'default', 'morning', true);
  validateTopic(value: any){
    if(value === "default"){
      this.topicHasError = true;
    } else{
      this.topicHasError = false
    }
  };
  onSubmit(){
    this.enrollmentService.enroll(this.userModel)
    .subscribe(
      data => console.log("Success!", data),
      error => this.errorMsg = error.statusText
    )
  }
}
